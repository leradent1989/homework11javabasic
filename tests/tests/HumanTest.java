package tests;

import com.danit.java.basic.homework11.domain.Family;
import com.danit.java.basic.homework11.domain.Fish;
import com.danit.java.basic.homework11.domain.Human;
import com.danit.java.basic.homework11.domain.Pet;
import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class
HumanTest {
    private Human module;
    private ByteArrayOutputStream output = new ByteArrayOutputStream();
    @Test
    public void  humanToString(){
        LocalDate dateLoc =  LocalDate.of(1973,10,23);
        long date = dateLoc.toEpochDay()*24*3600*1000;
        module = new Human("human","surname",(byte) 95,date);
        String actual = module.toString();
        String expected = "Human{name = human surname = surname birthDate = 23/10/1973 iq = 95 schedule = null} \n";
        assertEquals(expected,actual);
    }
    @Test

    public void testHumanGreetPet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mira = new Fish("Mira",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mira));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.greetPet(Mira);
        assertEquals(output.toString().replaceAll("\n",""),"Привет Mira","Successfully brings text");

        System.setOut(old);
    }
    @Test

    public void testHumanDescribePet(){
        PrintStream old=System.out;
        Human human=new Human();
        Human mother = new Human();
        Human father = new Human();
        Family Smith = new Family(mother,father);
        Pet Mura = new Fish("Mira",5,(byte) 60);
        Set pet = new HashSet<>(Set.of(Mura));
        Smith.setPet(pet);
        System.setOut(new PrintStream(output));
        human.describePet(Mura);
        assertEquals(output.toString().replaceAll("\n",""),"У меня есть FISH, ему 5 лет, он очень хитрый","Successfully brings text");

        System.setOut(old);
    }
}