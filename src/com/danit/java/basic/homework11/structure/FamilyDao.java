package com.danit.java.basic.homework11.structure;

import com.danit.java.basic.homework11.domain.Family;

import java.util.List;

 public interface FamilyDao {

    void save(Family family);

    boolean deleteFamily(int index);

    List<Family> findAll();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(Family family);

}
