package com.danit.java.basic.homework11.structure;

import com.danit.java.basic.homework11.domain.Family;

import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao(List<Family> familyList) {
        this.familyList = familyList;
    }

@Override
    public void save(Family family) {
        familyList.add(family);
    }
@Override
   public  boolean deleteFamily(int index){
        if(index <  0 || index > familyList.size()-1){
            return false;
        }else{

        familyList.remove(index);

            return true;
        }
   }
   @Override
    public  boolean deleteFamily(Family family){
        if(family == null){
            return false;}
        int length = familyList.size();
        familyList.remove(family);
        if(familyList.size() < length){
            return true;
        }else return false;
    }
    public List<Family> findAll() {
        return familyList;
    }
@Override
    public Family getFamilyByIndex(int index){
        if(index <0 || index > familyList.size()){
            return  null;
        } else return familyList.get(index);
    }

}
