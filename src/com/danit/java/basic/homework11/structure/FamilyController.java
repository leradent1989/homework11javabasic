package com.danit.java.basic.homework11.structure;

import com.danit.java.basic.homework11.FamilyOverflowException;
import com.danit.java.basic.homework11.domain.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;

    }

    public void start() throws ParseException {

        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Menu: ");
            System.out.println("1. Insert test data");
            System.out.println("2. Show all families ");
            System.out.println("3. Find families bigger then size ");
            System.out.println("4. Find families less then size");
            System.out.println("5. Count families with size");
            System.out.println("6. Create family");
            System.out.println("7. Delete family by index ");
            System.out.println("8. Edit family ");
            System.out.println("9. Delete children older then ");
            System.out.println("10. Exit");
            System.out.println("You choice: ");

            Scanner scanner = new Scanner(System.in);

            if (!scanner .hasNextInt()) {
                System.out.println("Illegal input");
                continue;
            }
            int menuItem = scanner.nextInt();

            switch (menuItem) {
                case 1:
                    try {
                        long millisInDay = 24 * 3600 * 1000;
                        LocalDate dateLoc1 = LocalDate.of(1973, 10, 23);
                        long date1 = dateLoc1.toEpochDay() * millisInDay;
                        LocalDate dateLoc2 = LocalDate.of(1978, 9, 15);
                        long date2 = dateLoc2.toEpochDay() * millisInDay;
                        LocalDate dateLoc3 = LocalDate.of(2010, 3, 3);
                        long date3 = dateLoc3.toEpochDay() * millisInDay;
                        LocalDate dateLoc4 = LocalDate.of(2018, 8, 8);
                        long date4 = dateLoc4.toEpochDay() * millisInDay;

                      List<Family> families2 = new ArrayList<>(List.of(
                                new Family(new Human("gerge1", "grgr1", (byte) 110, date1), new Human("gdgd1", "dgdgd1", (byte) 110, date2)),
                                new Family(new Human("gerge2", "grgr2", (byte) 110, date1), new Human("gdgd2", "dgdgd2", (byte) 110, date2)),
                                new Family(new Human("gerge3", "grgr3", (byte) 110, date1), new Human("gdgd3", "dgdgd3", (byte) 110, date2)),
                                new Family(new Human("gerge4", "grgr4", (byte) 110, date1), new Human("gdgd4", "dgdgd4", (byte) 110, date2)),
                                new Family(new Human("gerge5", "grgr5", (byte) 110, date1), new Human("gdgd5", "dgdgd5", (byte) 110, date2)),
                                new Family(new Human("gerge6", "grgr6", (byte) 110, date1), new Human("gdgd6", "dgdgd6", (byte) 110, date2))
                        ));

                        families2.get(0).getChildren().add(new Human("Jake", "vvtrt", "20/12/2008", (byte) 110, Gender.Male));
                        families2.get(2).getChildren().add(new Human("Jane", "vvtrt", "20/12/2010", (byte) 110, Gender.Female));
                        Set <Pet> pets = new HashSet<>(Set.of(new Dog("snoopy",4,(byte) 50),new Dog("linda",7,(byte) 60) ));
                        families2.forEach(el ->el.setPet(pets));
                        families2.forEach(el -> familyService.addFamily(el));

                    } catch (NullPointerException er) {
                        er.printStackTrace();
                        System.err.println("Не удалось загрузить данные");
                    }
                    break;
                case 2:
                    try {
                        if(familyService.getAllFamilies().size() == 0){
                            throw new Exception("LIST IS EMPTY PLEASE INSERT TEST DATA(MENU ITEM 1.)");
                        }
                        familyService.displayAllFamilies();

                    }  catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (Exception err) {
                        err.printStackTrace();

                    }

                    break;
                case 3:

                    try {
                        if (familyService.getAllFamilies() == null) {
                            throw new NullPointerException();
                        }
                        System.out.println("Enter size");
                        int size = scan.nextInt();

                        if (familyService.getAllFamilies().size() == 0) {
                            throw new IndexOutOfBoundsException();
                        }

                        familyService.getFamiliesBiggerThen(size).forEach(el -> {
                            System.out.println((familyService.getAllFamilies().indexOf(el)+1));
                            el.prettyFormat();
                        });
                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (IndexOutOfBoundsException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                   catch (InputMismatchException e){
                        e.printStackTrace() ;
                        System.err.println("  Введите целое число ");
                    }

                        break;

                case 4:
                    try{
                        if(familyService.getAllFamilies() == null){
                            throw new NullPointerException();
                        }
                        System.out.println("Enter size");
                        int size2 = scan.nextInt();

                        if(familyService.getAllFamilies().size() == 0){
                            throw new IndexOutOfBoundsException();
                        }
                        List<Family> list = familyService.getFamiliesLessThen(size2);
                        list.forEach(el -> {
                            System.out.println((familyService.getAllFamilies().indexOf(el)+1));

                            el.prettyFormat();});

                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (IndexOutOfBoundsException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                  catch (InputMismatchException e) {
                    e.printStackTrace() ;
                      System.err.println("  Введите целое число ");
                    }
                        break;

                case 5:
                    try{
                        if(familyService.getAllFamilies().size() == 0){
                            throw new NullPointerException();
                        }
                    System .out.println("Enter size");
                    int size3 = scan.nextInt();
                        if(familyService.getAllFamilies().size() == 0){
                            throw new IndexOutOfBoundsException();
                        }
                    System.out.println("Families with size " + size3+ " people : " + familyService.countFamiliesWithMemberNumber(size3));}
                    catch (InputMismatchException err){
                        err.printStackTrace();
                        System.err.println("  Введите целое число ");
                    }
                    catch (IndexOutOfBoundsException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    break;
                case 6:
                    try{

                        System.out.println("input mother name ");
                        String motherName = scan.nextLine();

                        System.out.println("input mother surname ");
                        String motherSurname = scan.nextLine();

                        System.out.println("input mother birth date(dd/MM/YYYY ");

                        String motherBirthDate = scan.nextLine();

                        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(motherBirthDate);
                        long birthDateMother = date.getTime();

                        System.out.println("Input father name");
                        String fatherName = scan.nextLine();
                        System.out.println("Input father surname");
                        String fatherSurname = scan.nextLine();

                        System.out.println("Input father birth date (dd/MM/YYYY)");

                        String fatherBirthDate = scan.nextLine();
                        Date  date2 = new SimpleDateFormat("dd/MM/yyyy").parse(fatherBirthDate);
                        long birthDateFather = date2.getTime();

                        familyService.createNewFamily(new Human(motherName, motherSurname, (byte) 110, birthDateMother), new Human(fatherName, fatherSurname, (byte) 110, birthDateFather));
                        familyService.displayAllFamilies();}
                    catch (ParseException e){
                        e.printStackTrace();
                    }catch (NullPointerException err){
                        err.printStackTrace();
                    }
                    break;
                case 7:

                    try {
                        if(familyService.getAllFamilies().size() == 0){
                            throw new NullPointerException();
                        }
                        System.out.println("Please enter index of family");
                        int familyIndex = scan.nextInt();
                        System.out.println("Family you are going to delete: \n");
                        familyService.getFamilyById(familyIndex).prettyFormat();
                        familyService.deleteFamilyByIndex(familyIndex);
                        System.out.println("Family successfully deleted -there is no such family in list: \n ".toUpperCase(Locale.ROOT));
                        familyService.displayAllFamilies();
                    }
                    catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                   } catch (InputMismatchException e) {
                        System.err.println("Index is too big");
                       System.err.println("INVALID FORMAT Please try again\n");

                    }
                        break;

                case 8:
                    try {
                        if(familyService.getAllFamilies().size() == 0){
                            throw new NullPointerException();
                        }

                        System.out.println("1.Born child");
                        System.out.println("2.Adopt child");
                        System.out.println("3.Return to main menu");
                        System.out.println("Your choice:");
                        int menuChoice = scan.nextInt();
                        switch (menuChoice) {
                            case 1:
                                System.out.println("Please enter index  of family");
                                int familyIndex3 = scan.nextInt();
                                Family family = familyService.getFamilyById(familyIndex3);
                                if (family.countFamily(family.getChildren()) > 7) {
                                    throw new FamilyOverflowException("Family size more then " + FamilyOverflowException.familySize);
                                }
                            scan.nextLine();
                            System.out.println("Please enter name if boy");
                            String boyName = scan.nextLine();

                            System.out.println("Please enter name if girl");

                            String girlName = scan.nextLine();
                            familyService.bornChild(family, girlName, boyName).prettyFormat();
                              break;
                            case 2:
                            System.out.println("Please enter index  of family");
                            int familyIndex4 = scan.nextInt();
                            Family family2 = familyService.getFamilyById(familyIndex4);
                            System.out.println("Please enter child name");
                            scan.nextLine();
                            String adoptChildName = scan.nextLine();
                            System.out.println("Please enter child  surname ");
                            String adoptChildSurname = scan.nextLine();
                            System.out.println("Please enter child  birthDate(dd/MM/yyyy) ");
                            String adoptChildAge = scan.nextLine();
                            System.out.println("Please enter child  gender ");
                            String adoptChildGender = scan.nextLine();
                            System.out.println("Please enter child  iq (less then 100) ");
                            byte adoptChildIq = scan.nextByte();

                            familyService.adoptChild(family2, new Human(adoptChildName, adoptChildSurname, adoptChildAge, adoptChildIq, (adoptChildGender.equals("female") ? Gender.Female : Gender.Male))).prettyFormat();
                            break;
                            case 3:
                                break;
                        }
                    }
                    catch (FamilyOverflowException err){
                        err.printStackTrace();
                    } catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }

                    break;
                case 9:
                    try{
                    System.out.println("Enter child age");
                    int childAge = scan.nextInt();
                    familyService.deleteAllChildrenOlderThen(childAge);
                    familyService.displayAllFamilies();}
                    catch (InputMismatchException err){
                        err.printStackTrace();
                        System.err.println(" Введите целое число");
                    } catch (NullPointerException e) {
                        System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
                        e.printStackTrace();

                    }
                    break;
                case 10:
                    System.exit(0);
                    break;

                default:
                    System.out.println("Illegal input");
            }

        }

    }
}