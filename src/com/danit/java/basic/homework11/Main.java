package com.danit.java.basic.homework11;

import com.danit.java.basic.homework11.domain.Family;
import com.danit.java.basic.homework11.structure.CollectionFamilyDao;
import com.danit.java.basic.homework11.structure.FamilyController;
import com.danit.java.basic.homework11.structure.FamilyDao;
import com.danit.java.basic.homework11.structure.FamilyService;

import java.text.ParseException;
import java.util.*;

public class Main {

    public static void main(String[] args) throws ParseException {

        List<Family> families = new ArrayList< >();

        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);
      familyController.start();
        }


    }
